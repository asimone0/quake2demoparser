
    function getFile() {
        document.getElementById("file").click();
    }

    function sub(obj) {
        var file = obj.value;
        var fileName = file.split("\\");
        document.getElementById("fileselectBtn").innerHTML = fileName[fileName.length - 1];
        event.preventDefault();
    }

    if (sessionStorage.getItem("{{.SessionName}}")) {
        sessionStorage.removeItem("{{.SessionName}}");
        window.location.reload(true);
    }