sessionStorage.setItem("{{.SessionName}}", "True");


// Table highlight function based on
// http://jsfiddle.net/kboucher/cH8Ya/

function markColumnCeilings(table) {
    console.log("enter")
    if (table === null) return;

    var tbody = table.tBodies[0],
        rowCount = tbody.rows.length,
        colCount = tbody.rows[0].cells.length,
        maxvalues = new Array(colCount),
        i = rowCount - 1,
        j = colCount - 1,
        cell, value;

    // loops through rows/columns to get col ceilings
    for (; i > -1; i--) {

        for (; j > -1; j--) {

            cell = tbody.rows[i].cells[j];
            value = parseFloat(cell.innerHTML);

            if (value.toString() === "NaN") continue;
            if (value > 0 && value >= (maxvalues[j] === undefined ? -1 : maxvalues[j])) {
                // Store highest values indexed by column
                maxvalues[j] = value; 
            }

        }

        j = colCount - 1;

    }

    // reset counters
    i = rowCount - 1;
    j = colCount - 1;

    // loops through rows/columns to set class based on high values
    for (; i > -1; i--) {

        for (; j > -1; j--) {

            cell = tbody.rows[i].cells[j];
            value = parseFloat(cell.innerHTML);

            if (value.toString() === "NaN") continue;
            if (value > 0 && value >= (maxvalues[j] === undefined ? -1 : maxvalues[j])) {
                tbody.rows[i].cells[j].setAttribute("class", "max");
            }

        }

        j = colCount - 1;

    }


}
$(function() {
    var table = document.getElementById('ctf');
    markColumnCeilings(table);

    var table = document.getElementById('frag');
    markColumnCeilings(table);

    var table = document.getElementById('weapons');
    markColumnCeilings(table);
});
