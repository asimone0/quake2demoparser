// Namecounter contains a data structure that is designed to
// hold a set of names and a count that increments automatically when
// Add() is called.
// It also contains the TupleList and Tuple data structures and helper
// functions that allow the collection to be sorted by name or count in
// ascending or descending order
package main

import "sort"

type NameCounter map[string]int

// Adds a name to the collection and increments the respective count
func (target NameCounter) Add(name string) {
	if target != nil {
		target[name] += 1
	}
}

// Creates and returns a new NameCounter, ready to use
func NewNameCounter() NameCounter {
	return make(map[string]int)
}

// Returns the name with the highest count
func (target NameCounter) Most() (string, int) {
	var mostname string
	var mostcount int
	for key, val := range target {
		if val > mostcount {
			mostname, mostcount = key, val
		}
	}
	return mostname, mostcount
}

// A simple structure that can represent an entry in a NameCounter
type NameCount struct {
	Name  string
	Count int
}

type NameCountSlice []NameCount

func (ncs NameCountSlice) Swap(i, j int) {
	ncs[i], ncs[j] = ncs[j], ncs[i]
}

func (ncs NameCountSlice) Len() int {
	return len(ncs)
}

func (ncs NameCountSlice) Less(i, j int) bool {
	return ncs[i].Count < ncs[j].Count
}

func (nc NameCounter) SortByCountDesc() NameCountSlice {
	ts := nc.NameCountSlice()
	sort.Sort(sort.Reverse(ts))
	return ts
}

func (nc NameCounter) SortByCountAsc() NameCountSlice {
	ts := nc.NameCountSlice()
	sort.Sort(ts)
	return ts
}

func (nc NameCounter) SortByNameAsc() NameCountSlice {
	names := nc.NameList()
	sort.Strings(names)
	return nc.fromNameOrder(names)
}

func (nc NameCounter) SortByNameDesc() NameCountSlice {
	names := nc.NameList()
	sort.Sort(sort.Reverse(sort.StringSlice(names)))
	return nc.fromNameOrder(names)
}

func (nc NameCounter) NameList() []string {
	var names []string
	for name := range nc {
		names = append(names, name)
	}
	return names
}

func (nc NameCounter) fromNameOrder(names []string) NameCountSlice {
	ncs := make(NameCountSlice, len(nc))
	for idx, name := range names {
		ncs[idx] = NameCount{name, nc[name]}
	}
	return ncs
}

func (nc NameCounter) NameCountSlice() NameCountSlice {
	ncs := make(NameCountSlice, len(nc))
	idx := 0
	for name, count := range nc {
		ncs[idx] = NameCount{name, count}
		idx += 1
	}
	return ncs
}
