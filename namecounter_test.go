package main

import (
	"testing"
)

func TestAdd(t *testing.T) {
	nc := NewNameCounter()
	nc.Add("test")
	nc.Add("test")
	if nc["test"] != 2 {
		t.Fatalf("NameCounter does not conatin the correct value expected 2 but was %d\n", nc["test"])
	}
}

func TestMost(t *testing.T) {
	nc := NewNameCounter()
	nc.Add("test0")
	nc.Add("test0")
	nc.Add("test0")
	nc.Add("test0")
	nc.Add("test1")
	nc.Add("test1")
	nc.Add("test2")
	nc.Add("test2")
	nc.Add("test2")
	nc.Add("test3")
	res, cnt := nc.Most()
	if res != "test0" || cnt != 4 {
		t.Fatalf("Most does not yield the correct value, expected \"test0\", 4 but was %s, %d\n", res, cnt)
	}
}

func TestSortByNameAsc(t *testing.T) {
	nc := NewNameCounter()
	nc.Add("zeta")
	nc.Add("zeta")
	nc.Add("zeta")
	nc.Add("zeta")
	nc.Add("alpha")
	nc.Add("alpha")
	nc.Add("gamma")
	nc.Add("gamma")
	nc.Add("gamma")
	nc.Add("gamma")

	ncs := nc.SortByNameAsc()

	if ncs[0].Name != "alpha" {
		t.Fatalf("SortByNameAsc does not yield the correct value, expected %q but was %s\n", "alpha", ncs[0].Name)
	}
}
