package main

import (
	"html/template"
	"io"
	"sort"
	"strings"

	"bytes"
)

const SESSION_NAME = "parsepagevisited"

type ParseResult struct {
	SessionName string
	MapName     string
	Teams       []*Team
	Players     []*Player
}

var players []*Player
var redTeam, blueTeam *Team
var kills, deaths int
var previousKiller string
var lfire bool

func init() {
	phrases = []string{
		BLASTER_KILL,
		SHOTGUN_KILL,
		SSG_KILL,
		MACHINEGUN_KILL,
		CHAINGUN_KILL,
		GRENADE_LAUNCHER_KILL_1,
		GRENADE_LAUNCHER_KILL_2,
		ROCKET_KILL_1,
		ROCKET_KILL_2,
		HYPERBLASTER_KILL,
		RAILGUN_KILL,
		BFG_KILL_1,
		BFG_KILL_2,
		BFG_KILL_3,
		GRENADE_KILL_1,
		GRENADE_KILL_2,
		GRENADE_KILL_3,
		TELEFRAG_KILL,
		FLAG_CAPTURE,
		FLAG_DROP,
		FLAG_STEAL,
		FLAG_STEAL_LFIRE,
		ASSIST,
		ASSIST_FRAG,
		ASSIST_DEFEND,
		ASSIST_RETURN,
		DEFEND,
		RETURN,
		RETURN_HELP,
		EFC_KILL,
		EFC_KILL_LFIRE,
		SUICIDE_BFG,
		SUICIDE_DEFAULT,
		SUICIDE_GRENADE_1,
		SUICIDE_GRENADE_2,
		SUICIDE_KILL,
		SUICIDE_ROCKET,
		DEATH_BARREL,
		DEATH_FALLING,
		DEATH_HURT,
		DEATH_LASER,
		DEATH_LAVA,
		DEATH_SANK,
		DEATH_SLIME,
		DEATH_SQUISH}

}

func ParseToTemplate(contents []byte, islfire bool, mapname string, w io.Writer, t *template.Template) {
	// reset every parse
	players = make([]*Player, 0)
	blueTeam = NewTeam(TeamColorBlue)
	redTeam = NewTeam(TeamColorRed)

	lfire = islfire
	str := hexStringFromData(contents)
	delimiter := "\n"
	items := strings.Split(str, delimiter)
	for _, line := range items {
		trimmed := strings.TrimSpace(line)
		if len(trimmed) > 1 {
			trim := trimmed[1:]

			if isTeamLine(trim) {
				setTeamForLine(trim)
			} else if isTeamSetupLine(trim) {
				findTeamForLine(trim)
			} else {

				var endRange int
				if lfire {
					endRange = strings.Index(trim, "Timelimit Hit")
				} else {
					endRange = strings.Index(trim, "Defense MVP")
				}

				if endRange != -1 {
					break
				} else {
					eventType(trim)
				}

			}
		}
	}

	activePlayers := make([]*Player, 0)
	for _, p := range players {
		if p.Kills+p.Killed > 0 {
			activePlayers = append(activePlayers, p)
		}
	}

	sort.Sort(sort.Reverse(ByScore(activePlayers)))
	t.Execute(w, &ParseResult{SESSION_NAME, mapname, []*Team{redTeam, blueTeam}, activePlayers})
}

func hexStringFromData(data []byte) string {
	var out bytes.Buffer
	for _, v := range data {
		v = (v & 0xFF)
		if v > 127 {
			v -= 128
		}
		if v != 0 {
			out.WriteByte(v)
		}
	}
	return string(out.Bytes())
}

func findTeamForLine(fileline string) {
	parts := strings.Split(fileline, " ")
	for _, str := range parts {

		var idx int
		if lfire {
			idx = strings.Index(str, "/ctf_")
		} else {
			idx = strings.Index(str, "/rb-")
		}

		if idx != -1 {
			// \x05 == char ENQ
			nameArray := strings.Split(str, "\x05")
			for _, n := range nameArray {
				slashidx := strings.Index(n, "\\")
				if slashidx != -1 {
					name := n[:slashidx]
					p := findOrCreatePlayer(name)
					coloridx := strings.Index(n, "rb-")
					if coloridx != -1 {
						colorletter := n[coloridx+3 : coloridx+4]
						if colorletter == "b" {
							addPlayerToTeam(p, blueTeam)
						} else {
							addPlayerToTeam(p, redTeam)
						}
					}
				}
			}
		}
	}
}

func findOrCreatePlayer(name string) *Player {
	var currentPlayer *Player
	for _, p := range players {
		if p.Name() == name {
			currentPlayer = p
			break
		}
	}
	if currentPlayer == nil {
		currentPlayer = NewPlayer(name)
		players = append(players, currentPlayer)
	}
	return currentPlayer
}

func setTeamForLine(fileline string) {
	if i := strings.Index(fileline, TEAM_ID_PHRASE); i != -1 {
		name := strings.TrimSpace(fileline[:i])
		teamEnding := strings.TrimSpace(fileline[i+len(TEAM_ID_PHRASE):])
		p := findOrCreatePlayer(name)
		if teamEnding == RED_TEAM_LINE_END {
			addPlayerToTeam(p, redTeam)
		} else {
			addPlayerToTeam(p, blueTeam)
		}
	}
}

func addPlayerToTeam(p *Player, team *Team) {
	// make sure player is not on both teams at the same time
	if team.Color() == TeamColorRed {
		blueTeam.RemovePlayer(p)
		redTeam.AddPlayer(p)
	} else {
		redTeam.RemovePlayer(p)
		blueTeam.AddPlayer(p)
	}
}

func isTeamLine(fileline string) bool {
	return strings.Contains(fileline, TEAM_ID_PHRASE)
}

func isTeamSetupLine(fileline string) bool {

	if lfire {
		return strings.Contains(fileline, "/ctf_")
	} else {
		return strings.Contains(fileline, "/rb-")
	}

}

func eventType(fileline string) {
	for _, phrase := range phrases {
		if strings.Contains(fileline, phrase) {
			createEntryForPhrase(fileline, phrase)
			break
		}
	}
}

func createEntryForPhrase(fileline, phrase string) {
	eventType := EventTypeIgnore

	var victim string
	var killer string

	if phrase == SSG_KILL {
		victim = parseFirstPlayer(fileline, phrase)
		killer = parseSecondPlayer(fileline, phrase, "'s")
		eventType = EventTypeKillSuperShotgun
		previousKiller = killer
	} else if phrase == ROCKET_KILL_1 || phrase == ROCKET_KILL_2 {
		victim = parseFirstPlayer(fileline, phrase)
		killer = parseSecondPlayer(fileline, phrase, "'s")
		eventType = EventTypeKillRocketLauncher
		previousKiller = killer
	} else if phrase == RAILGUN_KILL {
		victim = parseFirstPlayer(fileline, phrase)
		killer = parseSecondPlayer(fileline, phrase, "")
		eventType = EventTypeKillRailgun
		previousKiller = killer
	} else if phrase == GRENADE_LAUNCHER_KILL_1 || phrase == GRENADE_LAUNCHER_KILL_2 {
		victim = parseFirstPlayer(fileline, phrase)
		killer = parseSecondPlayer(fileline, phrase, "'s")
		eventType = EventTypeKillGrenadeLauncher
		previousKiller = killer
	} else if phrase == CHAINGUN_KILL {
		victim = parseFirstPlayer(fileline, phrase)
		killer = parseSecondPlayer(fileline, phrase, "'s")
		eventType = EventTypeKillChaingun
		previousKiller = killer
	} else if phrase == HYPERBLASTER_KILL {
		victim = parseFirstPlayer(fileline, phrase)
		killer = parseSecondPlayer(fileline, phrase, "'s")
		eventType = EventTypeKillHyperBlaster
		previousKiller = killer
	} else if phrase == TELEFRAG_KILL {
		victim = parseFirstPlayer(fileline, phrase)
		killer = parseSecondPlayer(fileline, phrase, "'s")
		eventType = EventTypeKillTelefrag
		previousKiller = killer
	} else if phrase == MACHINEGUN_KILL {
		victim = parseFirstPlayer(fileline, phrase)
		killer = parseSecondPlayer(fileline, phrase, "")
		eventType = EventTypeKillMachinegun
		previousKiller = killer
	} else if phrase == BLASTER_KILL {
		victim = parseFirstPlayer(fileline, phrase)
		killer = parseSecondPlayer(fileline, phrase, "")
		eventType = EventTypeKillBlaster
		previousKiller = killer
	} else if phrase == GRENADE_KILL_1 || phrase == GRENADE_KILL_2 || phrase == GRENADE_KILL_3 {
		victim = parseFirstPlayer(fileline, phrase)
		killer = parseSecondPlayer(fileline, phrase, "'s")
		eventType = EventTypeKillGrenade
		previousKiller = killer
	} else if phrase == SHOTGUN_KILL {
		victim = parseFirstPlayer(fileline, phrase)
		killer = parseSecondPlayer(fileline, phrase, "")
		eventType = EventTypeKillShotgun
		previousKiller = killer
	} else if phrase == BFG_KILL_1 || phrase == BFG_KILL_2 || phrase == BFG_KILL_3 {
		victim = parseFirstPlayer(fileline, phrase)
		killer = parseSecondPlayer(fileline, phrase, "'s")
		eventType = EventTypeKillBFG
		previousKiller = killer
	} else {
		killer = parseFirstPlayer(fileline, phrase)
		if phrase == FLAG_STEAL || phrase == FLAG_STEAL_LFIRE {
			eventType = EventTypeSteal
		} else if phrase == FLAG_CAPTURE {
			eventType = EventTypeCapture
		} else if phrase == ASSIST || phrase == ASSIST_DEFEND || phrase == ASSIST_RETURN {
			defendidx := strings.Index(fileline, "defending")
			returnidx := strings.Index(fileline, "returning")
			if defendidx != -1 {
				eventType = EventTypeAssistDefend
			} else if returnidx != -1 {
				eventType = EventTypeAssistReturn
			} else {
				eventType = EventTypeAssistKill
			}
		} else if phrase == SUICIDE_DEFAULT || phrase == SUICIDE_BFG || phrase == SUICIDE_GRENADE_1 || phrase == SUICIDE_GRENADE_2 || phrase == SUICIDE_KILL || phrase == SUICIDE_ROCKET {
			eventType = EventTypeSuicide
		} else if phrase == EFC_KILL || phrase == ASSIST_FRAG {
			eventType = EventTypeEFCKill
		} else if phrase == FLAG_DROP {
			eventType = EventTypeDrop
			if lfire {
				trackEventType(EventTypeEFCKill, previousKiller, victim)
			}
		} else if phrase == DEFEND {
			aggidx := strings.Index(fileline, "aggressive enemy")
			fcidx := strings.Index(fileline, "flag carrier")
			baseidx := strings.Index(fileline, "base")
			if aggidx != -1 {
				eventType = EventTypeDefendAggressive
			} else if fcidx != -1 {
				eventType = EventTypeDefendFC
			} else if baseidx != -1 {
				eventType = EventTypeDefendBase
			} else {
				eventType = EventTypeDefendFlag
			}
		} else if phrase == RETURN {
			eventType = EventTypeReturn
		} else if phrase == RETURN_HELP {
			eventType = EventTypeReturnHelped
		} else if phrase == DEATH_BARREL || phrase == DEATH_FALLING || phrase == DEATH_HURT || phrase == DEATH_LASER || phrase == DEATH_LAVA || phrase == DEATH_SANK || phrase == DEATH_SLIME || phrase == DEATH_SQUISH {
			eventType = EventTypeDeath
		} else {
			return
		}
	}

	trackEventType(eventType, killer, victim)
}

func trackEventType(event QuakeLogEvent, activeplayer, passiveplayer string) {
	if event == EventTypeIgnore {
		return
	}
	active := findOrCreatePlayer(activeplayer)
	var passive *Player
	if passiveplayer != "" {
		passive = findOrCreatePlayer(passiveplayer)
		if active.Team() == nil {
			// log.Printf("No team for player: %s", active.Name)
			findTeamForPlayer(active, passive)
		}
	}
	switch event {
	case EventTypeKillBlaster:
		active.Kills++
		passive.Killed++
		active.Blaster++
		active.AddVictim(passive.Name())
		passive.AddEnemy(active.Name())
	case EventTypeKillShotgun:
		active.Kills++
		passive.Killed++
		active.Shotgun++
		active.AddVictim(passive.Name())
		passive.AddEnemy(active.Name())
	case EventTypeKillSuperShotgun:
		active.Kills++
		passive.Killed++
		active.Supershotgun++
		active.AddVictim(passive.Name())
		passive.AddEnemy(active.Name())
	case EventTypeKillMachinegun:
		active.Kills++
		passive.Killed++
		active.Machinegun++
		active.AddVictim(passive.Name())
		passive.AddEnemy(active.Name())
	case EventTypeKillChaingun:
		active.Kills++
		passive.Killed++
		active.Chaingun++
		active.AddVictim(passive.Name())
		passive.AddEnemy(active.Name())
	case EventTypeKillGrenadeLauncher:
		active.Kills++
		passive.Killed++
		active.Grenadelauncher++
		active.AddVictim(passive.Name())
		passive.AddEnemy(active.Name())
	case EventTypeKillRocketLauncher:
		active.Kills++
		passive.Killed++
		active.Rocketlauncher++
		active.AddVictim(passive.Name())
		passive.AddEnemy(active.Name())
	case EventTypeKillHyperBlaster:
		active.Kills++
		passive.Killed++
		active.Hyperblaster++
		active.AddVictim(passive.Name())
		passive.AddEnemy(active.Name())
	case EventTypeKillRailgun:
		active.Kills++
		passive.Killed++
		active.Railgun++
		active.AddVictim(passive.Name())
		passive.AddEnemy(active.Name())
	case EventTypeKillBFG:
		active.Kills++
		passive.Killed++
		active.Bfg++
		active.AddVictim(passive.Name())
		passive.AddEnemy(active.Name())
	case EventTypeKillGrenade:
		active.Kills++
		passive.Killed++
		active.Grenade++
		active.AddVictim(passive.Name())
		passive.AddEnemy(active.Name())
	case EventTypeKillTelefrag:
		active.Kills++
		passive.Killed++
		active.Telefrag++
		active.AddVictim(passive.Name())
		passive.AddEnemy(active.Name())
	case EventTypeAssistKill:
		active.AssistKill++
	case EventTypeAssistDefend:
		active.AssistDefend++
	case EventTypeAssistReturn:
		active.AssistReturn++
	case EventTypeDefendFlag:
		active.DefendsFlag++
	case EventTypeDefendBase:
		active.DefendsBase++
	case EventTypeDefendAggressive:
		active.DefendsAggressive++
	case EventTypeDefendFC:
		active.DefendsFlagCarrier++
	case EventTypeDrop:
		active.Drops++
	case EventTypeEFCKill:
		active.Efckills++
	case EventTypeReturn:
		active.ReturnsFlag++
	case EventTypeReturnHelped:
		active.ReturnsHelped++
	case EventTypeSteal:
		active.Steals++
	case EventTypeCapture:
		active.Caps++
	case EventTypeDeath:
		active.Deaths++
	case EventTypeSuicide:
		active.Suicides++
	}
}

func findTeamForPlayer(player, enemyPlayer *Player) {
	if enemyPlayer.Team() != nil {
		if enemyPlayer.IsRedTeam() == true {
			player.SetTeam(blueTeam)
			addPlayerToTeam(player, blueTeam)
		} else {
			player.SetTeam(redTeam)
			addPlayerToTeam(player, redTeam)
		}
	}
}

func parseFirstPlayer(fileline, phrase string) string {
	if idx := strings.Index(fileline, phrase); idx != -1 {
		return strings.TrimSpace(fileline[:idx])
	} else {
		return ""
	}
}

func parseSecondPlayer(fileline, phrase, indicator string) string {
	phraseidx := strings.Index(fileline, phrase)
	var indicatoridx int
	if indicator != "" {
		indicatoridx = strings.Index(fileline, indicator)
	} else {
		indicatoridx = len(fileline)
	}

	if phraseidx == -1 || indicatoridx == -1 {
		return ""
	}

	return strings.TrimSpace(fileline[phraseidx+len(phrase) : indicatoridx])

}
