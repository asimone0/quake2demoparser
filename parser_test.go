package main

import (
	"io/ioutil"
	"os"
	"path"
	"testing"
)

func TestHexStringMatchesiOSOutput(t *testing.T) {
	hexStringTestHelper("lm32-mr-fu", t)
	hexStringTestHelper("smap04-df-fu", t)
}

// hexStringTestHelper performs the initial hexStringFromData conversion
// from the iOS code and verifies that the output matches the output from the iOS code
func hexStringTestHelper(fileroot string, t *testing.T) {
	in := path.Join("testfiles", "input", fileroot+".dm2")

	testfile, err := os.Open(in)
	if err != nil {
		t.Fatalf("could not open %s", in)
	}
	rawinput, err := ioutil.ReadAll(testfile)
	str := hexStringFromData(rawinput)

	iosfilename := path.Join("testfiles", "output", "ios_"+fileroot+"-file-hexstring.txt")

	iosfile, err := os.Open(iosfilename)
	ios, err := ioutil.ReadAll(iosfile)

	if str != string(ios) {
		t.Fatalf("hexStringFromData for %s does not match iOS output", fileroot)
	}
}
