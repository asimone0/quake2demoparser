package main

var phrases []string

const (
	BLASTER_KILL            = "was blasted by"
	SHOTGUN_KILL            = "was gunned down by"
	SSG_KILL                = "was blown away by"
	MACHINEGUN_KILL         = "was machinegunned by"
	CHAINGUN_KILL           = "was cut in half by"
	GRENADE_LAUNCHER_KILL_1 = "was popped by"
	GRENADE_LAUNCHER_KILL_2 = "was shredded by"
	ROCKET_KILL_1           = " ate "
	ROCKET_KILL_2           = "almost dodged"
	HYPERBLASTER_KILL       = "was melted by"
	RAILGUN_KILL            = "was railed by"
	BFG_KILL_1              = "saw the pretty lights from"
	BFG_KILL_2              = "was disintegrated by"
	BFG_KILL_3              = "couldn't hide from"
	GRENADE_KILL_1          = "caught"
	GRENADE_KILL_2          = "didn't see"
	GRENADE_KILL_3          = "feels"
	TELEFRAG_KILL           = "tried to invade"
	FLAG_STEAL              = "stole"
	FLAG_STEAL_LFIRE        = "got the"
	FLAG_CAPTURE            = "captured"
	ASSIST                  = "assisted"
	ASSIST_DEFEND           = "against an aggressive enemy"
	ASSIST_FRAG             = "gets an assist for fragging the flag carrier!"
	ASSIST_RETURN           = "gets an assist for returning the flag!"
	FLAG_DROP               = "lost the"
	EFC_KILL                = "killed the enemy flag carrier"
	EFC_KILL_LFIRE          = "BONUS"
	DEFEND                  = "defends"
	RETURN_HELP             = "helped"
	RETURN                  = "returned"
	SUICIDE_GRENADE_1       = "tried to put the pin back in"
	SUICIDE_GRENADE_2       = "tripped on"
	SUICIDE_ROCKET          = "blew"
	SUICIDE_BFG             = "should have used a smaller gun"
	SUICIDE_KILL            = "killed"
	SUICIDE_DEFAULT         = "suicides"
	DEATH_FALLING           = "cratered"
	DEATH_SQUISH            = "was squished"
	DEATH_SANK              = "sank like a rock"
	DEATH_SLIME             = "melted"
	DEATH_LAVA              = "does a back flip into the lava"
	DEATH_BARREL            = "blew up"
	DEATH_LASER             = "saw the light"
	DEATH_HURT              = "was in the wrong place"
	TEAM_ID_PHRASE          = "is now on the"
	RED_TEAM_LINE_END       = "red team."
)
