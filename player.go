package main

import (
	"fmt"
	"math"
	"strings"
)

type KillStruct struct {
	KillsFor int
	KilledBy int
}

type Player struct {
	name               string
	team               *Team
	Caps               int
	AssistKill         int
	AssistDefend       int
	AssistReturn       int
	ReturnsFlag        int
	ReturnsHelped      int
	DefendsFlag        int
	DefendsBase        int
	DefendsAggressive  int
	DefendsFlagCarrier int
	Efckills           int
	Steals             int
	Drops              int
	Kills              int
	Killed             int
	Suicides           int
	Deaths             int
	Blaster            int
	Shotgun            int
	Supershotgun       int
	Machinegun         int
	Chaingun           int
	Grenadelauncher    int
	Rocketlauncher     int
	Hyperblaster       int
	Railgun            int
	Bfg                int
	Grenade            int
	Telefrag           int
	HotStreak          int
	currentKillStreak  int
	victims            NameCounter
	enemies            NameCounter
}

func (player *Player) SetTeam(team *Team) {
	player.team = team
}

func (player *Player) Team() *Team {
	return player.team
}

func (player *Player) Name() string {
	return player.name
}

func (player *Player) ScoreMinusCaps() int {
	return player.Score() - player.Caps
}

func (player *Player) IsRedTeam() bool {
	if player.team == nil {
		return false
	}
	return player.team.Color() == TeamColorRed
}

func NewPlayer(name string) *Player {
	p := new(Player)
	p.name = name
	p.enemies = NewNameCounter()
	p.victims = NewNameCounter()
	return p
}

func (player *Player) Score() int {
	if player.team == nil {
		return 0
	}
	teamCapScore := player.team.Caps() * 10
	return (player.Kills + (player.Caps * 5) + (player.DefendsFlagCarrier * 2) + (player.DefendsFlag * 2) + (player.DefendsAggressive * 3) + player.DefendsBase + player.Assists() + player.Returns() + (player.Efckills * 2) - player.Suicides - player.Deaths + teamCapScore)
}

func (player *Player) CtfScore() int {
	return (player.Kills + (player.Caps * 5) + (player.DefendsFlagCarrier * 2) + (player.DefendsFlag * 2) + (player.DefendsAggressive * 3) + player.DefendsBase + player.Assists() + player.Returns() + (player.Efckills * 2) - player.Suicides - player.Deaths)
}

func (player *Player) MaxHotStreak() int {
	if player.Killed == 0 {
		return player.currentKillStreak
	}
	if player.currentKillStreak > player.HotStreak {
		return player.currentKillStreak
	}
	return player.HotStreak
}

func (player *Player) Kdratio() float32 {
	numberOfRoundingPlaces := 2
	kdr := float32(player.Kills) / float32(player.Killed)
	pow := math.Pow(10, float64(numberOfRoundingPlaces))
	digit := pow * float64(kdr)
	round := math.Ceil(digit)

	return float32(round / pow)
}

func (player *Player) TeamName() string {
	if player.IsRedTeam() {
		return "Red"
	} else {
		return "Blue"
	}
}

func (player *Player) Assists() int {
	return player.AssistDefend + player.AssistKill + player.AssistReturn
}

func (player *Player) Defends() int {
	return player.DefendsAggressive + player.DefendsBase + player.DefendsFlag + player.DefendsFlagCarrier
}

func (player *Player) Returns() int {
	return player.ReturnsFlag + player.ReturnsHelped
}

func (player *Player) AddVictim(victim string) {
	player.victims.Add(victim)
	player.currentKillStreak++
}

func (player *Player) AddEnemy(enemy string) {
	player.enemies.Add(enemy)
	if player.currentKillStreak > player.HotStreak {
		player.HotStreak = player.currentKillStreak
	}
	player.currentKillStreak = 0
}

func (player *Player) WorstEnemy() string {
	result, count := player.enemies.Most()
	return fmt.Sprintf("%s (%d)", result, count)
}

func (player *Player) EasiestVictim() string {
	result, count := player.victims.Most()
	return fmt.Sprintf("%s (%d)", result, count)
}

func (player *Player) SortedKillArray() []string {
	ska := make([]string, len(player.victims))
	ts := player.victims.SortByCountDesc()
	for i, v := range ts {
		ska[i] = v.Name
	}
	return ska
}

func (player *Player) KillsForName(name string) int {
	return player.victims[name]
}

func (player *Player) KilledByName(name string) int {
	return player.enemies[name]
}

func (player *Player) FixName() string {
	firstpass := strings.Replace(player.name, "<", "", -1)
	return strings.Replace(firstpass, ">", "", -1)
}

func (player *Player) KillMap() map[string]KillStruct {
	km := make(map[string]KillStruct, 0)
	for _, name := range player.SortedKillArray() {
		km[name] = KillStruct{player.KillsForName(name), player.KilledByName(name)}
	}
	return km
}
