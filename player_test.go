package main

import (
	"testing"
)

func TestConstructor(t *testing.T) {
	p := NewPlayer("test")
	p.AddVictim("test victim")
	p.AddVictim("test victim")
	if p.Name() != "test" {
		t.Fatalf("p.Name() does not conatin the correct value expected %q but was %q\n", "test", p.Name())
	}
}

func TestAddVictim(t *testing.T) {
	p := NewPlayer("test")
	p.AddVictim("test victim")
	p.AddVictim("test victim")
	if p.victims["test victim"] != 2 {
		t.Fatalf("p.victims does not conatin the correct value from AddVictim() expected 2 but was %d\n", p.victims["test victim"])
	}
}

func TestAddEnemy(t *testing.T) {
	p := NewPlayer("test")
	p.AddEnemy("test enemy")
	p.AddEnemy("test enemy")
	if p.enemies["test enemy"] != 2 {
		t.Fatalf("p.enemies does not conatin the correct value from AddEnemy() expected 2 but was %d\n", p.enemies["test enemy"])
	}
}

func TestIncrement(t *testing.T) {
	p := NewPlayer("test")
	p.Kills++
	p.Kills++
	p.Kills++
	if p.Kills != 3 {
		t.Fatalf("p.Kills did not increment correctly, expected 3 but was %d\n", p.Kills)
	}
}
