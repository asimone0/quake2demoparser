// +build appengine

package main

import (
	"html/template"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"

	"appengine"
	"appengine/blobstore"
	"appengine/datastore"
)

type InputTemplate struct {
	SessionName string
	UploadUrl   *url.URL
}

func serveError(c appengine.Context, w http.ResponseWriter, err error) {
	w.WriteHeader(http.StatusInternalServerError)
	w.Header().Set("Content-Type", "text/plain")
	io.WriteString(w, "Internal Server Error")
	c.Errorf("%v", err)
}

var rootTemplate = template.Must(template.ParseFiles("template/indextmpl.html"))
var resultsTemplate = template.Must(template.ParseFiles("template/resultstmpl.html"))

func handleRoot(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	uploadURL, err := blobstore.UploadURL(c, "/upload", nil)
	if err != nil {
		serveError(c, w, err)
		return
	}
	w.Header().Set("Content-Type", "text/html")
	err = rootTemplate.Execute(w, &InputTemplate{SESSION_NAME, uploadURL})
	if err != nil {
		c.Errorf("%v", err)
	}
}

func handleServe(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	blobkey := appengine.BlobKey(r.FormValue("blobKey"))

	lfire := r.FormValue("lfire") != ""

	br := blobstore.NewReader(c, blobkey)
	blobinfo, _ := blobstore.Stat(c, blobkey)
	filename := blobinfo.Filename
	bites, _ := ioutil.ReadAll(br)
	w.Header().Set("Content-Type", "text/html")
	w.Header().Set("Cache-Control", "private, no-store, max-age=0, no-cache, must-revalidate, post-check=0, pre-check=0")
	w.Header().Set("Pragma", "no-cache")
	w.Header().Set("Expires", "Fri, 01 Jan 1990 00:00:00 GMT")
	ParseToTemplate(bites, lfire, filename, w, resultsTemplate)

}

func handleUpload(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	blobs, vals, err := blobstore.ParseUpload(r)
	if err != nil {
		serveError(c, w, err)
		return
	}
	file := blobs["file"]
	if len(file) == 0 {
		c.Errorf("no file uploaded")
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	//key only query to check datastore for the MD5
	q := datastore.NewQuery("__BlobInfo__").Filter("md5_hash =", file[0].MD5).KeysOnly()

	keys, err := q.GetAll(c, nil)

	if err != nil {
		serveError(c, w, err)
		return
	}

	var blobKey string
	if len(keys) > 0 {
		// parser has seen this file before, use the previous file
		blobKey = keys[0].StringID()
		// delete the current blob because we don;t need it
		blobstore.Delete(c, file[0].BlobKey)
	} else {
		// new blob, use the current key
		blobKey = string(file[0].BlobKey)
	}

	lfireparam := ""
	if vals.Get("lfire") != "" {
		lfireparam = "&lfire=true"
	}
	http.Redirect(w, r, "/serve/?blobKey="+blobKey+lfireparam, http.StatusFound)
}

func init() {
	http.HandleFunc("/", handleRoot)
	http.HandleFunc("/serve/", handleServe)
	http.HandleFunc("/upload", handleUpload)
}
