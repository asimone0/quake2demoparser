package main

type QuakeLogEvent int

const (
	EventTypeKillBlaster QuakeLogEvent = iota
	EventTypeKillShotgun
	EventTypeKillSuperShotgun
	EventTypeKillMachinegun
	EventTypeKillChaingun
	EventTypeKillGrenadeLauncher
	EventTypeKillRocketLauncher
	EventTypeKillHyperBlaster
	EventTypeKillRailgun
	EventTypeKillBFG
	EventTypeKillGrenade
	EventTypeKillTelefrag
	EventTypeDefendFlag
	EventTypeDefendBase
	EventTypeDefendAggressive
	EventTypeDefendFC
	EventTypeSteal
	EventTypeAssistKill
	EventTypeAssistDefend
	EventTypeAssistReturn
	EventTypeDrop
	EventTypeEFCKill
	EventTypeReturn
	EventTypeReturnHelped
	EventTypeCapture
	EventTypeSuicide
	EventTypeDeath
	EventTypeIgnore
)
