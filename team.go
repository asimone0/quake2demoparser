package main

import "fmt"

type TeamColor int

const (
	TeamColorRed TeamColor = iota
	TeamColorBlue
)

func (tc TeamColor) String() string {
	if tc == TeamColorBlue {
		return "Blue"
	} else {
		return "Red"
	}
}

type Team struct {
	color           TeamColor
	players         []*Player
	caps            int
	assists         int
	returns         int
	defends         int
	efckills        int
	steals          int
	drops           int
	kills           int
	killed          int
	score           int
	statsCalculated bool
}

func (team *Team) Color() TeamColor {
	return team.color
}

func (team *Team) Players() []*Player {
	return team.players
}

func (team *Team) String() string {
	return fmt.Sprintf("%#v", team)
}

func NewTeam(tc TeamColor) *Team {
	team := &Team{color: tc}
	team.players = make([]*Player, 0)
	team.statsCalculated = false
	return team
}

func (team *Team) Caps() int {
	var res int
	for _, player := range team.players {
		if player.Kills+player.Killed > 0 {
			res += player.Caps
		}
	}
	return res
}

func (team *Team) Assists() int {
	var res int
	for _, player := range team.players {
		if player.Kills+player.Killed > 0 {
			res += player.Assists()
		}
	}
	return res
}
func (team *Team) Returns() int {
	var res int
	for _, player := range team.players {
		if player.Kills+player.Killed > 0 {
			res += player.Returns()
		}
	}
	return res
}
func (team *Team) Defends() int {
	var res int
	for _, player := range team.players {
		if player.Kills+player.Killed > 0 {
			res += player.Defends()
		}
	}
	return res
}
func (team *Team) Efckills() int {
	var res int
	for _, player := range team.players {
		if player.Kills+player.Killed > 0 {
			res += player.Efckills
		}
	}
	return res
}

func (team *Team) Steals() int {
	var res int
	for _, player := range team.players {
		if player.Kills+player.Killed > 0 {
			res += player.Steals
		}
	}
	return res
}
func (team *Team) Drops() int {
	var res int
	for _, player := range team.players {
		if player.Kills+player.Killed > 0 {
			res += player.Drops
		}
	}
	return res
}
func (team *Team) Kills() int {
	var res int
	for _, player := range team.players {
		if player.Kills+player.Killed > 0 {
			res += player.Kills
		}
	}
	return res
}
func (team *Team) Killed() int {
	var res int
	for _, player := range team.players {
		if player.Kills+player.Killed > 0 {
			res += player.Killed
		}
	}
	return res
}

func (team *Team) Score() int {
	var res int
	for _, player := range team.players {
		if player.Kills+player.Killed > 0 {
			res += player.Score()
		}
	}
	return res
}
func (team *Team) CalculateStats() {
	if !team.statsCalculated {
		for _, player := range team.players {
			if player.Kills+player.Killed > 0 {
				team.caps += player.Caps
				team.assists += player.Assists()
				team.returns += player.Returns()
				team.defends += player.Defends()
				team.efckills += player.Efckills
				team.steals += player.Steals
				team.drops += player.Drops
				team.kills += player.Kills
				team.killed += player.Killed
				team.score += player.Score()
			}
		}
		team.statsCalculated = true
	}
}

func (team *Team) Name() string {
	return team.color.String()
}

func (team *Team) RemovePlayer(p *Player) {
	for i := 0; i < len(team.players); i++ {
		player := team.players[i]
		if p.Name() == player.Name() {
			copy(team.players[i:], team.players[i+1:])
			team.players[len(team.players)-1] = nil
			team.players = team.players[:len(team.players)-1]
			// log.Printf("Removing %s from %s team", p.Name, strings.ToLower(team.TeamColor.String()))
			break
		}
	}
}

func (team *Team) AddPlayer(p *Player) {
	p.SetTeam(team)
	for _, player := range team.players {
		if p.Name() == player.Name() {
			return
		}
	}
	team.players = append(team.players, p)
}

func (team *Team) StatsLine() string {
	team.CalculateStats()
	return fmt.Sprintf("<tr><td>%s</td>"+
		"<td>%d</td>"+
		"<td>%d</td>"+
		"<td>%d</td>"+
		"<td>%d</td>"+
		"<td>%d</td>"+
		"<td>%d</td>"+
		"<td>%d</td>"+
		"<td>%d</td>"+
		"<td>%d</td>"+
		"<td>%d</td></tr>",
		team.Name(),
		team.caps,
		team.assists,
		team.returns,
		team.defends,
		team.efckills,
		team.steals,
		team.drops,
		team.kills,
		team.killed,
		team.score)
}
