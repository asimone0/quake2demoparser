package main

import "testing"

func TestNewTeam(t *testing.T) {
	team := NewTeam(TeamColorBlue)
	if team.Name() != "Blue" {
		t.Fatalf("TeamColorBlue does not yield correct name, expected Blue but was %s\n", team.Name())
	}
}
